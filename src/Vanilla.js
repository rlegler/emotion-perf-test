import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";

class App extends Component {
    render() {
        return (
            <div className="canvas">
                {new Array(window.itemCount).fill(0).map((items, index) => (
                    <button key={index} className="button">
                        Vanilla
                    </button>
                ))}
            </div>
        );
    }
}

export default App;
