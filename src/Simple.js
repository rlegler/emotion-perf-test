import React, { Component } from "react";

import "./App.css";

import { observe } from "react-performance-observer";

class Simple extends Component {
    render() {
        return (
            <div className="canvas">
                {new Array(window.itemCount).fill(0).map((items, index) => (
                    <SimpleButton key={index} />
                ))}
            </div>
        );
    }
}

class SimpleButton extends React.Component {
    render() {
        return <button className="button">Simple</button>;
    }
}

export default Simple;
