import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";

import { ThemeProvider } from "emotion-theming";
import { Flexbox, Toggle, Button, colorSchemeLight } from "whats-the-matter-ui";

import { observe } from "react-performance-observer";
var itemCount = 100;
var t0 = performance.now();

observe(measurements => {
    // measurements.forEach(item => {
    // });
    var t1 = performance.now();
    const TimeToRender = t1 - t0;
    console.log("TimeToRender", TimeToRender);
    console.log("Total Items", itemCount);
    console.log("ms per component", TimeToRender / itemCount);
});

const items = new Array(itemCount).fill(0);

class App extends Component {
    render() {
        return (
            <div>
                {/* {items.map((items, index) => (
                    <button key={index} className="button">
                        css Button
                    </button>
                ))} */}

                {/* {items.map((items, index) => (
                    <SimpleButton key={index} />
                ))} */}

                {/* <ThemeProvider theme={colorSchemeLight}>
                    {items.map((items, index) => (
                        <Button key={index} type="primary" size="l">
                            Button
                        </Button>
                    ))}
                </ThemeProvider> */}
            </div>
        );
    }
}

class SimpleButton extends React.Component {
    render() {
        return <button className="button">css Button</button>;
    }
}

export default App;
