import React, { Component } from "react";

const Report = ({ data }) => {
    const DataDB = JSON.parse(localStorage.getItem("perf-test"));
    console.log("DataDB", DataDB);
    return (
        <ul>
            <div className="table-row" key="header">
                <div className="table-cell">Items</div>
                <div className="table-cell">Total Time</div>
                <div className="table-cell">Type</div>
                <div className="table-cell">Time Per Item</div>
            </div>
            {DataDB &&
                DataDB.map(item => (
                    <div className="table-row" key={item.timeStamp}>
                        <div className="table-cell">{item.itemCount}</div>
                        <div className="table-cell">{item.timeToRender}</div>
                        <div className="table-cell">{item.type}</div>
                        <div className="table-cell">{item.timePerItem}</div>
                    </div>
                ))}
        </ul>
    );
};

export default Report;
