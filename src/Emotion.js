import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";

import { ThemeProvider } from "emotion-theming";
import { Flexbox, Toggle, Button, colorSchemeLight } from "whats-the-matter-ui";

class Emotion extends Component {
    render() {
        return (
            <div className="canvas">
                <ThemeProvider theme={colorSchemeLight}>
                    {new Array(window.itemCount).fill(0).map((items, index) => (
                        <Button key={index} type="primary" size="l">
                            Emotion
                        </Button>
                    ))}
                </ThemeProvider>
            </div>
        );
    }
}

export default Emotion;
