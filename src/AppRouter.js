import React, { Component } from "react";
import Vanilla from "./Vanilla";
import Simple from "./Simple";
import Emotion from "./Emotion";
import Report from "./Report";

import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { observe } from "react-performance-observer";

const count = window.location.href.split("?")[1] || 100;
window.itemCount = count * 1;

var t0 = performance.now();

observe(measurements => {
    // measurements.forEach(item => {
    // });
    var t1 = performance.now();
    const timeToRender = t1 - t0;

    console.log("TimeToRender", timeToRender);
    console.log("Total Items", window.itemCount);
    const timePerItem = timeToRender / window.itemCount;
    console.log("timePerItem", timePerItem);

    // const path = window.location.pathname;
    // var type;

    // if (path === "/vanilla/") {
    //     type = "Vanilla";
    // } else if (path === "/simple/") {
    //     type = "Simple";
    // } else if (path === "/emotion/") {
    //     type = "Emotion";
    // }

    // const prev = JSON.parse(localStorage.getItem("perf-test-prev"));

    // if (type && prev !== type) {
    //     const data = {
    //         timeStamp: Date.now(),
    //         timeToRender,
    //         itemCount,
    //         timePerItem,
    //         type
    //     };
    //     const DataDB = JSON.parse(localStorage.getItem("perf-test"));
    //     if (DataDB) {
    //         DataDB.push(data);
    //         localStorage.setItem("perf-test", JSON.stringify(DataDB));
    //         localStorage.setItem("perf-test-prev", JSON.stringify(type));
    //     } else {
    //         localStorage.setItem("perf-test", JSON.stringify([]));
    //     }
    // }
});

const AppRouter = () => {
    const data = localStorage.getItem("perf-test");
    const param = window.location.href.split("?")[1] || 100;

    return (
        <Router>
            <div>
                <nav>
                    <ul>
                        <li>
                            <Link
                                onClick={() =>
                                    setTimeout(() => {
                                        window.location.reload();
                                    })
                                }
                                to={`/vanilla?${param}`}
                            >
                                Vanilla html/css Button
                            </Link>
                        </li>

                        <li>
                            <Link
                                onClick={() =>
                                    setTimeout(() => {
                                        window.location.reload();
                                    })
                                }
                                to={`/simple?${param}`}
                            >
                                Button Component w/ css
                            </Link>
                        </li>
                        <li>
                            <Link
                                onClick={() =>
                                    setTimeout(() => {
                                        window.location.reload();
                                    })
                                }
                                to={`/emotion?${param}`}
                            >
                                Emotion Button Component
                            </Link>
                        </li>

                        {/* <li>
                            <Link to="/">report</Link>
                        </li> */}
                    </ul>
                </nav>

                {/* <Route path="/" component={Report} /> */}

                <Route path="/vanilla/" exact component={Vanilla} />
                <Route path="/simple/" exact component={Simple} />
                <Route path="/emotion/" exact component={Emotion} />
            </div>
        </Router>
    );
};

export default AppRouter;
